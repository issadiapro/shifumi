package itakademy.fr;

public class Application {
    public static void main (String args[]) {
        Joueur  joueur1 = new Joueur("Bill");
        Joueur  joueur2 = new Joueur("Jean");
         Partie partie = new Partie(joueur1, joueur2);
         partie.initialize();
         partie.game();

    }
}
