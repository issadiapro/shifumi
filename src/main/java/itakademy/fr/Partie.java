package itakademy.fr;

import java.util.Scanner;

public class Partie {
    Joueur joueur1;
    Joueur joueur2;
    int round;

    public Partie(Joueur joueur1,Joueur joueur2) {
        this.joueur1 = joueur1;
        this.joueur2 = joueur2;
    }

    public Joueur getJoueur1() {
        return joueur1;
    }

    public Joueur getJoueur2() {
        return joueur2;
    }

    public void setJoueur1(Joueur joueur1) {
        this.joueur1 = joueur1;
    }

    public void setJoueur2(Joueur joueur2) {
        this.joueur2 = joueur2;
    }


    public void initialize() {
        joueur1.setScore(0);
        joueur2.setScore(0);
        System.out.print("Partie initialisée \n"); 

    }

    public void game() {
        int round =1;
        Scanner scan= new Scanner(System.in);
        int joueur1_score = joueur1.getScore();
        int joueur2_score = joueur2.getScore();
        while(round <=3) {
            System.out.print("Joueur 1 : Veuillez choisir un coup parmi cette liste 1.Pierre 2.Feuille 3.Ciseaux \n"); 
            int player1_choice= scan.nextInt();
            System.out.print("Joueur 2 : Veuillez choisir un coup parmi cette liste 1.Pierre 2.Feuille 3.Ciseaux \n");
            int player2_choice = scan.nextInt();
            if (player1_choice == player2_choice) {
                System.out.print("pas de vainqueur pour cette manche \n");
                joueur1_score ++;
                joueur2_score ++;

            } else if (player1_choice +1 == player2_choice) {
                System.out.print("Le joueur 1 a remporté cette manche \n");
                 joueur1_score ++;

            } else if (player1_choice -1 == player2_choice) {
                System.out.print("Le joueur 2 a remporté cette manche \n");
                joueur2_score ++;

            }  else if (player2_choice == 1 && player1_choice == 3) { 
                System.out.print("Le joueur 2 a remporté cette manche \n");
                joueur2_score ++;
             } else if(player2_choice == 3 && player1_choice == 1) {
                 System.out.print("Le joueur 1 a remporté cette manche \n");
                joueur1_score ++;
             }
             round++;
        }

        System.out.print("Fin de la partie \n");
        if (joueur1_score > joueur2_score) {
            System.out.print("Le joueur 1 a gagné la partie! \n");
        } else if (joueur1_score == joueur2_score) {
            System.out.print("Egalité à la fin des 3 manches \n");

        }
        
        else {
            System.out.print("Le joueur 2 a gagné la partie! \n");
        }

    }

}
