package itakademy.fr;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PartieTest {

    private Partie partie;
    private Joueur joueur1;
    private Joueur joueur2;

    @BeforeEach
    public void init() {
        this.joueur1 = new Joueur("Test");
        this.joueur2 = new Joueur("Test");
        this.partie = new Partie(joueur1, joueur2);
    }

    @Test
    void updateScoreJoueur1() {
        this.joueur1.setScore(1);
        assertEquals(1, this.joueur1.getScore());
    }

     @Test
    void updateScoreJoueur2() {
        this.joueur2.setScore(1);
        assertEquals(1, this.joueur2.getScore());
    }


}
